package webapp.basic.servlets;


import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import webapp.basic.security.UserService;
import webapp.basic.security.RequestHandling;

import java.util.ArrayList;
import java.util.List;

public class ServletRouter {
        private final List<Class<? extends AbstractRoutableHttpServlet>> servletClasses = new ArrayList<>();

        {
            servletClasses.add(HomeServlet.class);
            servletClasses.add(LoginServlet.class);
            servletClasses.add(LogoutServlet.class);
            servletClasses.add(AddUserServlet.class);
            servletClasses.add(EditUserServlet.class);
            servletClasses.add(RemoveServlet.class);
            servletClasses.add(RegisterServlet.class);
            servletClasses.add(ForwardServlet.class);
        }

        public void init(Context ctx){

            UserService userService = new UserService();
            RequestHandling requestHandling = new RequestHandling();
            requestHandling.setUserService(userService);

            for(Class<? extends AbstractRoutableHttpServlet> servletClass: servletClasses){
                try {
                    AbstractRoutableHttpServlet httpServlet = servletClass.newInstance();
                    httpServlet.setRequestHandling(requestHandling);
                    Tomcat.addServlet(ctx, servletClass.getSimpleName(), httpServlet);
                    ctx.addServletMapping(httpServlet.getPattern(), servletClass.getSimpleName());
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

}
