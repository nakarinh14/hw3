package webapp.basic.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AddUserServlet extends AbstractRoutableHttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (requestHandling.isAuthorized(request)) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/add_user.jsp");
            requestDispatcher.include(request, response);

        } else {
            request.setAttribute("error", "You are not authorized to do add user.");
            response.sendRedirect("/login");

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(requestHandling.isAuthorized(request)){
            if(requestHandling.addUser(request)){
                response.sendRedirect("/home");
            } else{
                request.setAttribute("user", requestHandling.getUser(request));
                request.setAttribute("info", "User edit failed. Please check if username is new and all info are in correct format.");
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/add_user.jsp");
                requestDispatcher.include(request, response);
            }
        } else {
            request.setAttribute("error", "Session expired. Please login again.");
            response.sendRedirect("/login");
        }
    }

    @Override
    public String getPattern() {
        return "/add";
    }
}
