package webapp.basic.servlets;

import webapp.basic.security.RequestHandling;

import javax.servlet.http.HttpServlet;

public abstract class AbstractRoutableHttpServlet extends HttpServlet implements Routable {

    protected RequestHandling requestHandling;

    public void setRequestHandling(RequestHandling requestHandling){
        this.requestHandling = requestHandling;
    }

}
