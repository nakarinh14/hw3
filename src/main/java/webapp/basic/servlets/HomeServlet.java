package webapp.basic.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class HomeServlet extends AbstractRoutableHttpServlet {



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (requestHandling.isAuthorized(request)) {
            String username = requestHandling.getCurrentUsername(request);
            request.setAttribute("username", username);
            requestHandling.getUserList(request);

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/home.jsp");
            requestDispatcher.include(request, response);
        } else{
            response.sendRedirect("/login");
        }
    }

    @Override
    public String getPattern() {
        return "/home";
    }
}
