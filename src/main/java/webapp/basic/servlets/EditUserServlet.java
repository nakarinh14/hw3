package webapp.basic.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditUserServlet extends AbstractRoutableHttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (requestHandling.isAuthorized(request)) {
            request.setAttribute("user", requestHandling.getUser(request));
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/edit_user.jsp");
            requestDispatcher.include(request, response);

        } else {
            request.setAttribute("error", "You are not authorized to do edit user.");
            response.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // authentication

        if(requestHandling.isAuthorized(request)){
            if(requestHandling.editUser(request)){
                response.sendRedirect("/");
            } else{
                request.setAttribute("user", requestHandling.getUser(request));
                request.setAttribute("info", "User edit failed. Please check if username doesn't exist yet and all info are in correct format.");
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/edit_user.jsp");
                requestDispatcher.include(request, response);
            }
        } else {
            request.setAttribute("error", "Session expired. Please login again.");
            response.sendRedirect("/login");
        }
    }

    @Override
    public String getPattern() {
        return "/edit";
    }
}
