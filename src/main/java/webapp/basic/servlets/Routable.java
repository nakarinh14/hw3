package webapp.basic.servlets;

public interface Routable {

    String getPattern();
}
