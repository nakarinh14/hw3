package webapp.basic.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class RequestHandling {

    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public String getCurrentUsername(HttpServletRequest request){
        HttpSession session = request.getSession();
        Object usernameObject = session.getAttribute("username");
        return (String) usernameObject;
    }

    public boolean isAuthorized(HttpServletRequest request){
        String username = getCurrentUsername(request);
        return userService.checkIfUserExists(username);

    }

    public User getUser(HttpServletRequest request){
        return userService.findByUsername(request.getParameter("username"));
    }

    public boolean addUser(HttpServletRequest request){
        Map<String, String> userInfo = new HashMap<>(); {{
            userInfo.put("username", request.getParameter("username"));
            userInfo.put("password", request.getParameter("password"));
            userInfo.put("firstname", request.getParameter("firstname"));
            userInfo.put("lastname", request.getParameter("lastname"));
        }}
        return userService.addUser(userInfo);
    }

    public boolean editUser(HttpServletRequest request){

        Map<String, String> userInfo = new HashMap<>(); {{
            userInfo.put("original", request.getParameter("original-name"));
            userInfo.put("username", request.getParameter("username"));
            userInfo.put("password", request.getParameter("password"));
            userInfo.put("firstname", request.getParameter("firstname"));
            userInfo.put("lastname", request.getParameter("lastname"));
        }}
        return userService.editUser(userInfo);
    }


    public void logout(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.removeAttribute("username");
        session.invalidate();
    }

    public boolean login(HttpServletRequest request){
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if(userService.authenticatePassword(username, password)){
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            return true;
        }
        return false;
    }

    public void removeUser(HttpServletRequest request){

        if(request.getParameter("username") != null) {
            String username = request.getParameter("username");
            userService.removeUser(username);
        }
    }

    public void getUserList(HttpServletRequest request){
        request.setAttribute("lst", userService.getAllUsers());
    }

}
