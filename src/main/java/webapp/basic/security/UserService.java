package webapp.basic.security;

import org.springframework.security.crypto.bcrypt.BCrypt;
import webapp.basic.database.FetchDatabase;

import java.util.*;

public class UserService {


    private Map<String, User> users = FetchDatabase.getAllRows();

    private void updateUsers(){
        users = FetchDatabase.getAllRows();
    }

    public User findByUsername(String username){
        return users.get(username);
    }

    public Collection<User> getAllUsers(){
        return users.values();
    }

    public boolean checkIfUserExists(String username){
        return users.containsKey(username);
    }

    public void removeUser(String username){
        if(FetchDatabase.removeUser(username)){
            updateUsers();
        }
    }

    public boolean editUser(Map<String, String> userInfo){

        String hashedPassword = BCrypt.hashpw(userInfo.get("password"), BCrypt.gensalt(12));
        userInfo.put("password", hashedPassword);
        if(FetchDatabase.editUser(userInfo)){
            updateUsers();
            return true;
        }
        return false;
    }

    public boolean addUser(Map<String, String> userInfo){
        String hashedPassword = BCrypt.hashpw(userInfo.get("password"), BCrypt.gensalt(12));
        userInfo.put("password", hashedPassword);
        if(FetchDatabase.addUser(userInfo)){
            updateUsers();
            return true;
        }
        return false;
    }

    public boolean authenticatePassword(String username, String password){
        User user = findByUsername(username);
        if(user != null) {
            return BCrypt.checkpw(password, user.getPassword());
        }
        return false;
    }

}
