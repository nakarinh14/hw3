package webapp.basic.database;

import java.sql.*;

public class QueryAuthentication {


    protected static boolean ifUserExist(String username, Connection conn)  {
        boolean cond = false;
        if(conn != null){
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM user WHERE username = '" +username +"'");
                if(rs.next()){
                    cond = true;
                    rs.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return cond;
    }

    protected static boolean validateUsername(Connection conn, String username) {
        return !ifUserExist(username, conn) && username.trim().length() > 0; // check if user doesnt exist, and not whitespace
    }

    protected static boolean validateEditInfo(Connection conn, String changeName, String originalName){
        if(!changeName.equals(originalName)){
            return validateUsername(conn, changeName);
        }
        return true;
    }

    protected static void executeQuery(String query, String[] parameters, Connection conn){

        try {
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            for(int i = 0; i < parameters.length; i++){
                preparedStmt.setString(i+1, parameters[i]);
            }
            preparedStmt.execute();
            conn.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
