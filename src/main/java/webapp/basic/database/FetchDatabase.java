package webapp.basic.database;

import webapp.basic.security.User;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class FetchDatabase {

    private static Connection getConnection()  {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_lists",
                    "root", "Nakarinh198799");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    private static Map<String, User> convertSetToList(ResultSet rs) throws SQLException {
        Map<String, User> lst = new HashMap<>();
        while(rs.next()){
            lst.put(rs.getString(2), new User(
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5)));
        }
        rs.close();
        return lst;
    }

    public static Map<String, User> getAllRows(){
        try {
            Connection conn = getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM user");
            return convertSetToList(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean removeUser(String username){
        String query = "DELETE FROM user WHERE username = ?";
        String[] parameters = {username};

        Connection conn = getConnection();
        if(QueryAuthentication.ifUserExist(username, conn)){
            QueryAuthentication.executeQuery(query, parameters, conn);
            return true;
        }
        return false;
    }

    public static boolean addUser(Map<String, String> userInfo){

        String query = "INSERT INTO user (username, password, firstname, lastname)" + " VALUES (?, ?, ?, ?)";
        String[] parameters = {
                userInfo.get("username"),
                userInfo.get("password"),
                userInfo.get("firstname"),
                userInfo.get("lastname"),
        };

        Connection conn = getConnection();

        if(QueryAuthentication.validateUsername(conn, userInfo.get("username"))){
            QueryAuthentication.executeQuery(query, parameters, conn);
            return true;
        }
        return false;
    }


    public static boolean editUser(Map<String, String> userInfo){

        String query = "UPDATE user SET username=?,password=?,firstname=?,lastname=? WHERE username=?";
        String[] parameters = {
                userInfo.get("username"),
                userInfo.get("password"),
                userInfo.get("firstname"),
                userInfo.get("lastname"),
                userInfo.get("original"),
        };

        Connection conn = getConnection();
        if(QueryAuthentication.validateEditInfo(conn, userInfo.get("username"), userInfo.get("original"))) {
            QueryAuthentication.executeQuery(query, parameters, conn);
            return true;
        }
        return false;
    }

}
